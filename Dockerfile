ARG SOURCE_DOCKER_REGISTRY=localhost:5000

FROM ${SOURCE_DOCKER_REGISTRY}/alpine_opt_samtools:1.10 AS opt_samtools

FROM ${SOURCE_DOCKER_REGISTRY}/alpine_base:3.11.3

COPY --from=opt_samtools /opt/bin/ /opt/bin/
COPY --from=opt_samtools /opt/lib/ /opt/lib/

ENV PATH /opt/bin/:/usr/bin/:/bin/:/usr/sbin/:/sbin/
ENV LD_LIBRARY_PATH /opt/lib/

ENTRYPOINT [ "/opt/bin/samtools" ]